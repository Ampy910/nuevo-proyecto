function getValuesNameDescriptionStatus() {
    const name = document.querySelector("#Name")
    const description = document.getElementById("Description")
    const status = document.getElementById("Stado")
    return {
        name : name.value,
        description : description.value,
        status :status.value
    }
}
const editTask = () => {
    console.log("editTask");
}


const addNewTask = () => {
    const newTask = getValuesNameDescriptionStatus()
    
    const task = `
        <div class="task">
            <h3 class="title">${newTask.name}</h3>
            <p class="text">${newTask.description}</p>
            <button class="btn" onclick="editTask()"> Edit</button>
        </div>
    `
    const contentTask = document.getElementById(`contentTask${newTask.status}`)

    contentTask.innerHTML += task

}


function onLoadEventClickAddTask() {
    const btnAddTask = document.querySelector("#btnAddTask")

    btnAddTask.addEventListener("click",addNewTask)
}

function onLoadAddTask() {
    onLoadEventClickAddTask()
}


window.addEventListener("load", onLoadAddTask)