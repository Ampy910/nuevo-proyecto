const toggelModal = () => {
  const modal = document.querySelector("#modalItem");
  modal.classList.toggle("active");
};

const loadActiveModal = () => {
  const btnActiveModal = document.querySelectorAll(".btnActiveModal");

  btnActiveModal.forEach((btn) => {
    btn.addEventListener("click", () => {
      toggelModal();
    });
  });
};

const loadModal = () => {
  loadActiveModal();
};

window.addEventListener("load", loadModal);
